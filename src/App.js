import { Component } from 'react';
import appTemplate from './appTemplate';

class App extends Component {
  render() {
    return appTemplate();
  }
}

export default App;
