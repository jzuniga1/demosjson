import React from 'react';
import './App.css';
import logo from './logo.svg';
import PrimerComponent from './Components/PrimerComponent';
import Bienvenida from './Components/Bienvenida';
import Persona from './Components/Persona';
import ListaFrutas from './Components/ListaFrutas';
import Color from './Components/Color';
import Contador from './Components/Contador';
import ToDoList from './Components/ToDoList';
import PreventDefault from './Components/PreventDefault';
import Edad from './Components/Edad';
import Peticion from './Components/Peticion';

export default () => {
    return (
        <div className="App">
            <div className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h2>Welcome to React</h2>
            </div>

            <div className="row">
                <div className="col">
                    <div className="App-intro">
                        <PrimerComponent />
                        <Bienvenida nombre="Vato" />
                        <Persona />
                        <ListaFrutas />
                        <Color color='#00ff00' mensaje="Verde" />
                        <Color color='#ff0000' mensaje="Rojo" />
                        <Contador />
                    </div>
                </div>

                <div className="col">
                    <ToDoList />
                    <PreventDefault />
                    <Edad />
                    <Peticion />
                </div>

                <div className="col">
                </div>

                <div className="col">
                </div>
            </div>
        </div>
    );
}