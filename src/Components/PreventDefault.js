import React, {Component} from "react";

/**
 * @class PreventDefault
 * @extends {Component}
 */
class PreventDefault extends Component {
	/**
     * @param {*} e
     * @memberof PreventDefault
     */
	defFunct(e) {
		e.preventDefault();
		console.log("Evento por defaultl...");
	}

	/**
     * @return {*}
     * @memberof PreventDefault
     */
	render() {
		return (
			<a href="" onClick={this.defFunct}>Evento default...</a>
		);
	}
}

export default PreventDefault;
