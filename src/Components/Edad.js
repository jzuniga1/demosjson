import React, {Component} from "react";
/**
 * @param  {String} props
 * @return {String} return
 */
function EdadValid(props) {
	if (props.esMayor >= 18) {
		return <p>Es mayor de edad ...</p>;
	} else {
		return <p>Es menor de edad ...</p>;
	}
}

/** Clase que recibe edad desde el imput. */
class Edad extends Component {
	/**
     * Estados react
     * @param {String} props
     */
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.state = {age: ""};
	}
	/**
	 * @param  {Event} e
	 */
	handleChange(e) {
		this.setState({age: e.target.value});
	}

	/**
	 * @return {String}
	 * @memberof Edad
	 */
	render() {
		let age = this.state.age;
		return (
			<div>
				<input className="form-control" value={age}
					onChange={this.handleChange} />
				<EdadValid esMayor={age} />
			</div>
		);
	}
}

export default Edad;
