import React, {Component} from 'react';
import List from './List';

/**
 * @class ToDoList
 * @extends {Component}
 */
class ToDoList extends Component {
    /**
     *Creates an instance of ToDoList.
     * @param {*} props
     * @memberof ToDoList
     */
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            items: []
        };
    }

    /**
     * @memberof ToDoList
     */
    onChange = (event) => {
        this.setState({text: event.target.value});
    }

    /**
     * @memberof ToDoList
     */
    add = (event) => {
        event.preventDefault();
        this.setState({
            text: '',
            items: [...this.state.items, this.state.text]
        });
    }

    /**
     * @return {*}
     * @memberof ToDoList
     */
    render() {
        return (
            <div className="App-intro">
                <h5>Lista de tareas</h5>
                <div>
                    <form onSubmit={this.onSubmit}>
                        <input className="form-control" placeholder="Items" value={this.state.text} onChange={this.onChange} />
                    </form>
                    <button className="btn btn-info btn-sm" onClick={this.add}>Agregar</button>
                    <div>
                        <List items={this.state.items} />
                    </div>
                </div>

            </div>
        )
    }
}

export default ToDoList