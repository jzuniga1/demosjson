import React, {Component} from "react";

/**
 * @class Color
 * @extends {Component}
 */
class Color extends Component {
	/**
     * @return
     * @memberof Color
     * @return {String}
     */
	render() {
		let colors = {
			color: this.props.color,
		};

		return (
			<div>
				<h3 style={colors}> {this.props.mensaje} </h3>
			</div>
		);
	}
}

export default Color;
