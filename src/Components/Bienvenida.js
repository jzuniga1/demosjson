import React, {Component} from "react";

/**
 * @class Bienvenida
 * @extends {Component}
 */
class Bienvenida extends Component {
	/**
     * @return
     * @memberof Bienvenida
     * @return {String}
     */
    render() {
		return (
			<div>
				<p> Que hongo ----- {this.props.nombre} </p>
			</div>
		);
	}
}

export default Bienvenida;
