import React, {Component} from "react";

/**
 * @class PrimerComponent
 * @extends {Component}
 */
class PrimerComponent extends Component {
	/**
     * @return {*}
     * @memberof PrimerComponent
     */
	render() {
		return (
			<div>
				<p>Mi primer componente</p>
			</div>
		);
	}
}

export default PrimerComponent;
