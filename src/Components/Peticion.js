import React, { Component } from 'react';
import axios from 'axios'

class Peticion extends Component {
    constructor() {
        super()
        this.state = {
            username: ''
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick() {
        axios.get('https://gitlab.com/jzuniga1/demosjson/blob/master/ObjTest.json')
            .then(response => this.setState({ username: response.data.nombre }))
    }

    render() {
        return (
            <div>
                <button className='btn btn-success btn-sm' onClick={this.handleClick}>
                    Peticion Request
                </button>
                <p>{this.state.username}</p>
            </div>
        )
    }
}

export default Peticion;