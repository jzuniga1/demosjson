import React, {Component} from 'react';

/**
 * @class Contador
 * @extends {Component}
 */
class Contador extends Component {
    /**
     *Creates an instance of Contador.
     * @memberof Contador
     */
    constructor() {
        super();
        this.state = {
            contador: 0
        };
    }

    /**
     * @memberof Contador
     */
    mas = () => {
        this.setState({contador: this.state.contador + 1})
    }

    /**
     * @memberof Contador
     */
    menos = () => {
        this.setState({contador: this.state.contador - 1})
    }

    /**
     * @returns
     * @memberof Contador
     */
    render() {
        return (
            <div>
                <p>Contador: {this.state.contador}</p>
                <button className="btn btn-success btn-sm" onClick={this.mas}>Más +</button>
                <button className="btn btn-danger btn-sm" onClick={this.menos}>Menos -</button>
            </div>
        )
    }
}

export default Contador;