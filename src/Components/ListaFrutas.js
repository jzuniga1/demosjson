import React, {Component} from "react";

/**
 * @class ListaFrutas
 * @extends {Component}
 */
class ListaFrutas extends Component {
	/**
     * @return {String}
     * @memberof ListaFrutas
     */
	render() {
		const lista = ["Manzana", "Pera", "Uva", "Naranja", "Limon", "Sandia"];
		let iteracion = lista.map((elemento, index) =>
			<li key={index}>
				{elemento}
			</li>
		);

		return (
			<div>
				<ul> {iteracion} </ul>
			</div>
		);
	}
}

export default ListaFrutas;
