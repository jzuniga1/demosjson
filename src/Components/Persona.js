import React, {Component} from "react";

/**
 * @class Persona
 * @extends {Component}
 */
class Persona extends Component {
	/**
     * @return {String}
     * @memberof Persona
     */
	render() {
		/**
         * @class Persona
         * @extends {React.Component}
         */
		class Persona extends React.Component {
			/**
             *Creates an instance of Persona.
             * @param {*} name
             * @param {*} edad
             * @param {*} color
             * @memberof Persona
             */
			constructor(name, edad, color) {
				super();
				this.name = name;
				this.edad = edad;
				this.color = color;
			}
		}

		let obj = new Persona("Jorge", 23, "Rojo");

		return (
			<div>
                Nombre: {obj.name}, Edad: {obj.edad}, Color: {obj.color}
			</div>
		);
	}
}

export default Persona;
